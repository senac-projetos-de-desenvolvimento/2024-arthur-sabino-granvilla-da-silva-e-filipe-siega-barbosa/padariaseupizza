import { Router } from "express";
import { clienteCreate, clienteIndex, clienteLogin,  } from "./controllers/clienteController.js";
import { padariaCreate, padariaDestaca, padariaDestaques, padariaIndex, padariaDestroy, PadariaUpdate } from "./controllers/PadariaController.js";
import { avaliacaoCreate, avaliacaoDestroy, avaliacaopadaria, avaliacaoGraphDias, avaliacaoGraphEstrelas, avaliacaoIndex, dadosGerais } from "./controllers/avaliacaoController.js";
import {adminCreate, adminLogin, logsCreate, obterTodosOsLogs } from "./controllers/admincontroller.js"


const router = Router();

// Clientes/Admins
router.get('/clienteslistar', clienteIndex)
      .post('/clientes', clienteCreate)
      .post('/login', clienteLogin)
      .get('/loginadmin', adminLogin)
      .post('/criarloginadmin', adminCreate)
     
      
      

      // Logs admins
      .post('/setlogs', logsCreate)
      .get('/logs', obterTodosOsLogs)

// Produtos
router.get('/api/padaria', padariaIndex)
      .get('/padaria/destaques', padariaDestaques)
      .post('/padariacreat', padariaCreate)
      .post('/padariaDestaca/:id', padariaDestaca)
      .delete('/padaria/destroy/:id', padariaDestroy)
      .put('/alterar/:id', PadariaUpdate)

// Avaliações
router.get('/avaliacoes', avaliacaoIndex)
      .post('/avaliacoescreat', avaliacaoCreate)
      .delete('/avaliacoes/:id', avaliacaoDestroy)
      .get('/avaliacoes/graph', avaliacaoGraphEstrelas)
      .get('/avaliacoes/graph_dias', avaliacaoGraphDias)
      .get('/avaliacoes/padaria/:padariaId', avaliacaopadaria);

router.get('/dados_gerais', dadosGerais);

export default router;
