**Padaria Seu Pizza**

**Este repositório contém a API para a Padaria Seu pizza, para o projeto de desenvolvimento I. Contendo uma aplicação focada no back-end.**

**Pré-requisitos**

**Antes de começar, certifique-se de ter o Node.js e os npm instalados.**

**Clonando o repositório**

# Clonar o projeto
```
git clone git@gitlab.com:senac-projetos-de-desenvolvimento/2024-arthur-sabino-granvilla-da-silva-e-filipe-siega-barbosa/padariaseupizza.git
```

# Entrar na pasta do projeto Padaria Seu Pizza
```
cd padariaseupizza
```

# Instalação das dependencias necessárias
```
npm install 
```

# Criação do banco de Dados
```
create database padaria;
```
# Assim você faz com que o seu back end seja executado
```
npx nodemon app
```
# Abrir o arquivo clonado pelo seu editor de código-fonte
```
code .
```