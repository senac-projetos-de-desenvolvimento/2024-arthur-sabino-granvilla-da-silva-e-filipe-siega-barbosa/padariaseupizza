import { Padaria } from '../models/padaria.js'

export const padariaIndex = async (req, res) => {
  try {
    const padarias = await Padaria.findAll()
    res.status(200).json(padarias)
  } catch (error) {
    res.status(400).send(error)
  }
}


export const padariaDestaques = async (req, res) => {
  try {
    const padarias = await Padaria.findAll({ where: { destaque: true } })
    res.status(200).json(padarias)
  } catch (error) {
    res.status(400).send(error)
  }
}

export const padariaDestaca = async (req, res) => {
  const { id } = req.params;

  if (!id) {
    return res.status(400).json({ error: "ID não fornecido" });
  }

  try {
    // posiciona no registro para obter o status atual do campo destaque
    const padaria = await Padaria.findByPk(id);

    if (!padaria) {
      return res.status(404).json({ error: "Padaria não encontrada" });
    }

    // altera com o contrário do atual
    padaria.destaque = !padaria.destaque;
    await padaria.save();

    res.status(200).json(padaria);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
};

export const padariaCreate = async (req, res) => {
  const { nome, preco, categoria, data } = req.body;


  if (!nome || !preco || !categoria || !data ) {
    return res.status(400).json({ id: 0, msg: "Erro... Informe todos os dados necessários" });
  }

  try {

    const existingProduct = await Padaria.findOne({ where: { nome } });
    if (existingProduct) {
      return res.status(400).json({ id: 0, msg: "Já existe um nome com esse nome" });
    }


    const padaria = await Padaria.create({
      nome, preco, categoria, data 
    });

    return res.status(201).json(padaria);
  } catch (error) {
    return res.status(400).send(error);
  }
}

export async function PadariaUpdate(req, res) {
  const { id } = req.params;
  const { nome, categoria, preco, data  } = req.body;

  if (!nome || !categoria || !preco || !data) {
    res.status(400).json({ error: "Erro... Informe nome, categoria, preço, dataicaçãoe descrição." });
    return;
  }

  try {
    const updatednome = await Padaria.update(
      {
        nome,
        categoria,
        preco,
        data,
    
        
      },
      {
        where: { id }
      }
    );

    if (updatednome[0] === 0) {
      res.status(404).json({ error: "nome não encontrado." });
    } else {
      res.status(200).json({ message: "nome atualizado com sucesso." });
    }
  } catch (error) {
    console.error("Erro ao atualizar o nome:", error);
    res.status(500).json({ error: "Erro ao atualizar o nome." });
  }
}

export const padariaDestroy = async (req, res) => {
  const { id } = req.params;

  try {
    if (!id) {
      return res.status(400).json({ error: "ID da padaria não fornecido" });
    }

    const padaria = await Padaria.findByPk(id);

    if (!padaria) {
      return res.status(404).json({ error: "Padaria não encontrada" });
    }

    await Padaria.destroy({ where: { id } });
    res.status(200).json({ msg: "Ok! Removido com Sucesso" });
  } catch (error) {
    console.error("Erro ao excluir a padaria:", error);
    res.status(500).json({ error: "Erro interno do servidor" });
  }
};
